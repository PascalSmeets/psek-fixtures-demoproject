package integration;


import fitnesse.junit.FitNesseRunner;
import fitnesse.junit.FitNesseRunner.ConfigFile;
import fitnesse.junit.FitNesseRunner.FitnesseDir;
import fitnesse.junit.FitNesseRunner.OutputDir;
import fitnesse.junit.FitNesseRunner.Suite;
import org.junit.runner.RunWith;


@RunWith(FitNesseRunner.class)

// Used to specify the testsuite/testcase that should be run
@Suite("TestSuite")

// Include ONLY test with the tag specified SuiteFilter
// use -DSuiteFilter=<TAGNAME> to specify
@FitNesseRunner.SuiteFilter("ci")
// Exclude ANY tests with the tag specified in ExcludeSuiteFilter
//@FitNesseRunner.ExcludeSuiteFilter("in progress")

@FitnesseDir(value = "src/test/resources/"
        , fitNesseRoot = "FitnesseRoot")
@OutputDir("target/fitnesse-test-results")
@ConfigFile("src/test/resources/plugins.properties")


// If you run from the commandline use 'mvn clean test -Pintegration-test -Dforkcount=1 -DskipTests=false -Drun=ci'
public class ciTest {}