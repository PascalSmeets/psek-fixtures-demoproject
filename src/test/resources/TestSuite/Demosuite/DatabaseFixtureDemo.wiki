---
Suites: demo, databasefixture, ci, regression
Test
---
!|script               |
|start|Database Fixture|
#
#
!3 !-<span class="label label-primary">Creating the connection | De verbinding maken</span>-!
#
#
In this example an in-memory H2 database is used | Dit voorbeeld maakt gebruik van een in-geheugen H2 database
!|script                                      |
|createConnection;|h2|demo.database.properties|
#
#
!*< hidden part used to fill database | verborgen, vullen van de database
!|script                          |
|executeSqlFile;|h2|demo.table.sql|
*!
#
#
!3 !-<span class="label label-primary">Commands for single results | Commando's voor losse resultaten</span>-!
!4 !-<span class="label label-info">Commands that return data | Commando's die gegevens teruggeven</span>-!
!|script                                                                                                     |
|$PersonsInGreenville=|performSelectSingleResult;|h2|SELECT COUNT(*) FROM Persons WHERE City = 'Greenville'  |
|show                 |performSelectSingleResult;|h2|SELECT COUNT(*) FROM Persons WHERE City = 'Greenville'  |
|check                |performSelectSingleResult;|h2|SELECT COUNT(*) FROM Persons WHERE City = 'Greenville'|4|
|$idOfJack=           |performSelectSingleResult;|h2|SELECT PersonID FROM Persons WHERE FirstName = 'Jack'   |
#
#
!4 !-<span class="label label-info">Getting CLOB data | Een CLOB veld uitlezen</span>-!
!|script                                                                                                                                                    |
|$remarksGrace=|getClobAsString;                                                  |h2         |SELECT Remarks FROM Persons WHERE FirstName = 'Grace'        |
|note          |look at the returned data when 'performSelectSingleResult' is used|vergelijk het resultaat met het resultaat van 'performSelectSingleResult'|
|$remarksGrace=|performSelectSingleResult;                                        |h2         |SELECT Remarks FROM Persons WHERE FirstName = 'Grace'        |
#
#
!4 !-<span class="label label-info">Polling in the database | Uitlezen totdat een resultaat gevonden is</span>-!
!|script                                                                                                                                                                                                                    |
|performSelectSingleResultUntilResultPresent;|h2                                          |SELECT Address FROM Persons WHERE FirstName = 'Lois'|regex:(Alder).*                                     |10                     |
|show                                        |performSelectSingleResultUntilResultPresent;|h2                                                  |SELECT Address FROM Persons WHERE FirstName = 'Fred'|regex:(Alder).*|10|5000|
#
#
!4 !-<span class="label label-info">Verify commands | Verify commando's</span>-!
!|script                                                                                           |
|verifyResultPerformSelectSingleResult;|h2|SELECT COUNT(*) FROM Persons WHERE City = 'Greenville'|4|
#
#
!3 !-<span class="label label-primary">Commands for result sets | Commando's voor resultaat sets</span>-!
!|script                                                              |
|performSelect;     |h2       |SELECT FirstName, LastName FROM Persons|
|activateResultSet; |h2                                               |
|show               |getNumberOfRows;                                 |
|verifyNumberOfRows;|4                                                |
|show               |getValue;|1            |FirstName                |
|show               |getValue;|1            |LastName                 |
|$firstName2=       |getValue;|2            |FirstName                |
|$lastName2=        |getValue;|2            |LastName                 |
|verifyValue;       |3        |FirstName    |Fred                     |
|verifyValue;       |3        |LastName     |Franks                   |
|verifyValue;       |4        |FirstName    |regex:[A-Z]{1}[a-z]{3}   |
|verifyValue;       |4        |LastName     |regexpi:lane             |
#
#
!3 !-<span class="label label-info">Executing SQL files / strings | Een SQL file uitvoeren</span>-!
the example for 'executeSqlFile' is hidden on the page, use edit mode and look after the createConnection part
het voorbeeld voor 'executeSqlFile' is verborgen op deze pagina, gebruik edit mode en kijk naar het blok code na het onderdeel createConnection
!|script                                                                                      |
|note             |check before              |controle vooraf                                 |
|check            |performSelectSingleResult;|h2     |SELECT COUNT(*) FROM Persons     |4     |
|executeSqlString;|h2                        |DELETE FROM Persons WHERE Address LIKE '%Alder%'|
|check            |performSelectSingleResult;|h2     |SELECT COUNT(*) FROM Persons     |2     |
#
#
!3 !-<span class="label label-info">Closing the connection | De verbinding sluiten</span>-!
!|script            |
|closeConnection;|h2|
