---
Suites: demo, variablesfixture, ci, regression
Test
---
!|script                |
|start|Variables Fixture|
#
#
!3 !-<span class="label label-primary">Returning a string | Teruggeven van een waarde</span>-!
#
#
!|script                                                                        |
|check            |giveText;|This is a string|This is a string                  |
|check not        |giveText;|This is a string|This is another string            |
|$text=           |giveText;|This is a string                                   |
|$moreText=       |giveText;|$text with some more text                          |
|$textWithPadding=|giveText;|!-    This is a string with whitespace padding   -!|
#
#
!3 !-<span class="label label-primary">Trimming a string | Spaties voor en achter verwijderen</span>-!
(use edit mode to see what this does | gebruik edit mode om te zien hoe dit werkt)
!|script                                                                                                                                                        |
|check    |trimText;|!-    This input string has whitespace padding (check in edit mode)  -!|This input string has whitespace padding (check in edit mode)      |
|$text=   |trimText;|!-    This input string has whitespace padding (check in edit mode)  -!                                                                    |
|check not|trimText;|!-    This input string has whitespace padding (check in edit mode)  -!|!- This input string has whitespace padding (check in edit mode) -!|
#
#
!3 !-<span class="label label-primary">Random data | Willekeurige gegevens</span>-!
#
#
!4 !-<span class="label label-info">Random entry from a set | Willekeurig item uit een lijst</span>-!
!define Fibonacci {1,1,2,3,5,8,13,21,34,55}
!define Avengers {Black Panther,Black Widow,Captain America,Captain Marvel,Falcon,Hank Pym,Hawkeye,Hulk,Iron Man,Luke Cage,Quicksilver,Scarlet Witch,Spider-Woman,Thor,Vision,Wasp,Wonder Man}
!define RandomButton {id=Search,//*[@id='Cancel'],id=Surpriseme}
#
!|script                     |
|show|giveEntry;|${Fibonacci}|
|show|giveEntry;|${Fibonacci}|
|show|giveEntry;|${Fibonacci}|
|show|giveEntry;|${Fibonacci}|
|show|giveEntry;|${Fibonacci}|
|show|giveEntry;|${Fibonacci}|
|show|giveEntry;|${Fibonacci}|
|show|giveEntry;|${Fibonacci}|
|show|giveEntry;|${Fibonacci}|
|show|giveEntry;|${Fibonacci}|
#
!|script                      |
|$name=|giveEntry;|${Avengers}|
|$name=|giveEntry;|${Avengers}|
|$name=|giveEntry;|${Avengers}|
|$name=|giveEntry;|${Avengers}|
|$name=|giveEntry;|${Avengers}|
|$name=|giveEntry;|${Avengers}|
|$name=|giveEntry;|${Avengers}|
|$name=|giveEntry;|${Avengers}|
|$name=|giveEntry;|${Avengers}|
|$name=|giveEntry;|${Avengers}|
#
!|script                                   |
|$buttonToClick=|giveEntry;|${RandomButton}|
|$buttonToClick=|giveEntry;|${RandomButton}|
|$buttonToClick=|giveEntry;|${RandomButton}|
|$buttonToClick=|giveEntry;|${RandomButton}|
|$buttonToClick=|giveEntry;|${RandomButton}|
|$buttonToClick=|giveEntry;|${RandomButton}|
|$buttonToClick=|giveEntry;|${RandomButton}|
|$buttonToClick=|giveEntry;|${RandomButton}|
|$buttonToClick=|giveEntry;|${RandomButton}|
|$buttonToClick=|giveEntry;|${RandomButton}|
#
#
!4 !-<span class="label label-info">Random number in a range | Willekeurig nummer in een bepaalde reeks</span>-!
!|script                             |
|$price=|giveRandomNumber;|1  |10    |
|$value=|giveRandomNumber;|100|100000|
|$value=|giveRandomNumber;|100|100000|
#
#
!4 !-<span class="label label-info">Random string with specified length | Willekeurige string van een bepaalde lengte</span>-!
!|script                           |
|$value=|giveSecureRandomString;|5 |
|$value=|giveSecureRandomString;|10|
|$value=|giveSecureRandomString;|25|
#
#
!4 !-<span class="label label-info">Random UUID | Willekeurig UUID</span>-!
!|script                |
|$value=|giveRandomUuid;|
|$value=|giveRandomUuid;|
|$value=|giveRandomUuid;|
#
#
!4 !-<span class="label label-info">Time based UUID | UUID op basis van de huidige tijd</span>-!
!|script                   |
|$value=|giveTimebasedUuid;|
|$value=|giveTimebasedUuid;|
|$value=|giveTimebasedUuid;|
#
#
!4 !-<span class="label label-info">Random string based on unix time | Willekeurig nummer gebaseerd op de unix tijd</span>-!
Because generation is fast it is possible to get the same string twice | Omdat het genereren snel gaat is het mogelijk dat dezelfde waarde gegenereerd wordt
!|script                       |
|$value=     |giveUniqueString;|
|$value=     |giveUniqueString;|
|threadSleep;|1                |
|$value=     |giveUniqueString;|
#
#
!4 !-<span class="label label-info">Random string based on unix time converted to letters | Willekeurig nummer gebaseerd op de unix tijd omgezet naar letters</span>-!
Because generation is fast it is possible to get the same string twice | Omdat het genereren snel gaat is het mogelijk dat dezelfde waarde gegenereerd wordt
!|script                             |
|$randomvalue=|giveUniqueAlphaString;|
|$value=      |giveUniqueAlphaString;|
|threadSleep; |1                     |
|$value=      |giveUniqueAlphaString;|
#
#
!3 !-<span class="label label-primary">Calculations | Rekenen</span>-!
!|script                        |
|check  |increaseValueByOne;|1|2|
|$value=|increaseValueByOne;|1  |
#
#
!|script           |
|check  |add;|1|2|3|
|$value=|add;|1|2  |
#
#
!|script                 |
|check  |subtract;|10|1|9|
|$value=|subtract;|10|1  |
#
#
!3 !-<span class="label label-primary">Concatenating strings | Samenvoegen van strings</span>-!
!|script                                                                     |
|addConcatItem;            |one                                              |
|addConcatItem;            |-                                                |
|addConcatItem;            |2                                                |
|addConcatItem;            |-                                                |
|addConcatItem;            |$randomvalue                                     |
|check                     |concat;|one-2-$randomvalue                       |
|addConcatItem;            |-                                                |
|addConcatItem;            |four                                             |
|$concatenation=           |concat;                                          |
|emptyConcatItems;                                                           |
|addConcatItem;            |5                                                |
|note                      |https://www.ssec.wisc.edu/~tomw/java/unicode.html|
|addConcatUnicodeCodepoint;|9733                                             |
|addConcatItem;            |'s                                               |
|show                      |concat;                                          |
|emptyConcatItems;                                                           |
|addConcatItem;            |!-the price is -!                                |
|addConcatUnicodeCodepoint;|8364                                             |
|addConcatItem;            |$price                                           |
|show                      |concat;                                          |